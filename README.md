# Abundance and distribution of *Legionellales* #

This repository is a data companion to the article "The all-host-adapted *Legionellales* are ubiquitous, rare and globally distributed". 

This information is provided as is, and no guarantee can be made on compatibility or runability on any system. It is meant to help other researchers and reviewers understand how the analysis was done.

## Files ##

### Scripts ###

* `doAbundanceSurvey.sh`: the main shell script containing most of the commands to gather data.
* `surveyAbundance.R`: all analyses in R, including generating figures from the paper.
* `abundanceSurvey.py`: gathering mostly OTU tables from EBI metagenomics
* `summarizeSample.py`: extracting metadata from EBI metagenomics, and calculating statistics from the OTU tables
* `add_temperatures.py`: completing the metadata with temperatures

### Data downloaded from online resources ###

* `allLegioRuns_180416.tab`: all runs containing *Legionellales* 
* `allRuns_180416.tab`: all runs at EBI metagenomics
* `allSamples_180416.tab`: all samples at EBI metagenomics
* `gg_13_8_taxonomy_legio.txt`: all OTUs belonging to *Legionellales* in greengeenes 13.8

### Lists and results ###

* `summary_v4.tab`: the result of the data crunching from EBI and the input to the R analysis.
* `biomes`: A list of all biomes present in the dataset, as well as the frequency and whether to keep them or to use the parent biome
* `biome_per_otu.tab`: The frequencies of each abundant OTU in different biomes
* `UpplandSamples.otuTable.ods` and `SalaSamples.otuTable.ods`: OTU tables for the Uppland and Sala samples, respectively. These files can be opened with LibreOffice.

### Figures ###

* `figures`: folder containing all figures. Figures are not named according to their actual number in the paper, but it should be easy to figure out which is which.

### Temporary or transitional files ###

* `allLegioRerun.tab`
* `missedRunIds`
* `noSuitableRunIds`
* `summary_bak.csv`
* `summary_cleaned.csv`
* `summary.tab`
* `summary_v3.tab`

## License ##

This repository is licensed under the terms of GNU General Public License v3.0.
