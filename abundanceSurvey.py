#!/usr/bin/env python
## Parses abundance (no of reads) from EBI metagenomics

## Libraries 
import csv
import sys
import os.path
#from pandas import DataFrame
import pandas
import urllib.request
from urllib.parse import urlencode, urlparse
from jsonapi_client import Session, Filter

# debug
from pprint import pprint

## Constants
API_BASE = 'https://www.ebi.ac.uk/metagenomics/api/latest/'
forceDownload = True
okDesc = ['OTUs, reads and taxonomic assignments', 'Reads encoding SSU rRNA']

## Parse options
scriptName = sys.argv.pop(0)
csvFile = sys.argv.pop(0)
outputDir = sys.argv.pop(0)
print('Running: ', scriptName, "\n  reading: ", csvFile, "\n  saving to ", outputDir, "\n", sep = '')

## Read all runs
df = pandas.read_table(filepath_or_buffer = csvFile, header = 0, quotechar = '"', skiprows = 1)
#print(df)

## Version scoring
def is_version_better(a, b):
  a_score = a
  b_score = b
  if a >= 2.0 and a < 4:
    a_score = a_score + 10
  if b >= 2.0 and b < 4:
    b_score = b_score + 10
  return a_score > b_score

## Foreach run, get OTU table
missed = []
nosuitable = []
with Session(API_BASE) as s:
  for runId in df['Run']:
    found = False
    print("Processing:", runId)
    otuTableFile = os.path.join(outputDir, runId + ".otu.tab")
    if os.path.exists(otuTableFile) and os.path.getsize(otuTableFile) > 0 and not forceDownload:
      # print("  OTU table already present at", otuTableFile)
      print("  OTU table already present for", runId)
      found = True
      
    else:
      print("  Finding otu tables")
      run = s.get('runs', runId).resource
      if len(run.analyses) > 1:
        print("  More than one analysis for", runId)
      else:
        print("  File found for", runId, "and only one version available")
        continue
      # TEMP HACK: enclose the whole stuff in here
      version  = 0
      # At best I want the highest between 3.1 and 2; then 4; then 1
      # Devise a score: if 2 < x < 3.9 then version +10; choose the
      # highest score
      best_version = 0
      print("  Searching best version")
      for a in run.analyses:
        version = float(a.pipeline_version)
        print("    Found version", version)
        if is_version_better(version, best_version):
          best_version = version

      print("  Best version:", best_version)
      for a in run.analyses:
        version = float(a.pipeline_version)
        if version == best_version:
          print("  Found best version", version)
          for d in a.downloads:
            # print('  ID: ', d.id,
            #     "\n  Description:", d.description.label,
            #     "\n  Type:", d.file_format.name,
            #     "\n  Link:", d.links.self)
            if d.description.label in okDesc and d.file_format.name == 'TSV':
              try:
                urllib.request.urlretrieve(d.links.self.href, otuTableFile)
                print("  Downloading file for version", version, "to",
                      otuTableFile)
                found = True
                break
              except:
                print("  URL Error for", runId)
                missed.append(runId)
          if found:
            print("  Finished for", runId)
            break
        else:
          print("  Not the right version:", version)
    if not found:
      print("  No suitable file for", runId)
      nosuitable.append(runId)
    print("  Finished for", runId)
          
## Write errors to a file 
with open('missedRunIds', 'a') as missedFile:
  for runId in missed:
    missedFile.write(runId + '\n')
with open('noSuitableRunIds', 'a') as nosuitableFile:
  for runId in nosuitable:
    nosuitableFile.write(runId + '\n')
