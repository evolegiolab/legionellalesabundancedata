#!/usr/bin/env python
""" Retrieve, post analysis, temperatures, and add them to the data frame
"""

## Libraries 
import sys
import os.path
import re
import pandas
import numpy as np
from jsonapi_client import Session, Filter

## Constants
API_BASE = 'https://www.ebi.ac.uk/metagenomics/api/latest/'
def format_unit(unit):
    import html
    return html.unescape(unit) if unit else ""

def get_temperature(sample_id, session):
  temperature = 'NA'
  unit = 'NA'
  try:
    sample = session.get('samples', sample_id).resource
    for m in sample.sample_metadata:
      if m['key'] == "temperature":
        temperature = m['value']
        unit = format_unit(m['unit'])
        break
    # else:
    #   print("No temperature for sample: " + str(sample_id))
  except:
    print("Something went wrong with sample: " + str(sample_id))
    
  return {'t':temperature, 'u':unit}

def main():
  # Args
  script_name = sys.argv.pop(0)
  sample_file = sys.argv.pop(0)
  output_file = sys.argv.pop(0)
  print("Running: %s\n  samples file: %s\n  write to: %s"
        % (script_name, sample_file, output_file))

  # Read samples
  samples = pandas.read_table(filepath_or_buffer = sample_file, header = 0,
                              quotechar = '"', quoting = 0,
                              doublequote = False,
                              skiprows = 0, index_col = 0)

  # Add two columns
  samples.insert(10, "temperature", "NA")
  #samples.insert(11, "temperature_unit", "NA")
  
  # Open session
  with Session(API_BASE) as session:

    # Process all samples
    for s in samples.itertuples():
      res = get_temperature(s[1], session)
      samples.loc[s[0], 'temperature'] = res['t']
      samples.loc[s[0], 'temperature_unit'] = res['u']

  samples = samples.replace(r'^NA$', np.nan, regex=True)
  samples.to_csv(output_file, sep="\t")

if __name__ == "__main__":
  main()
