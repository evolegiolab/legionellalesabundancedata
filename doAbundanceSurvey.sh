################################################################################
# Redo the EBI metagenomics analysis
################################################################################
# What I want is:
#  Sample: ID, project, study, Biome, Biome lineage, feature, material, lat, long; study name, abstract; 
#  Runs: sample, pipeline; total reads; total OTUs; frac Legio OTUs; frac Legio reads; id, reads and fraction for most abundant legioOTU, 

mkdir -p /nobackup/ebi/metagenomics/OTUtables


# Create virtual environment
python36 -m venv restApi
# activate
source restApi/bin/activate
# deactivate:
#deactivate

# for testing
head -n2 allLegioRuns_180416.tab > testRuns.tab
tail -n 1000 allLegioRuns_180416.tab | head -n 100  >> testRuns.tab
python abundanceSurvey.py testRuns.tab /nobackup/ebi/metagenomics/OTUtables

# Run the script: download all OTU tables with Legio samples
python abundanceSurvey.py allLegioRuns_180416.tab /nobackup/ebi/metagenomics/OTUtables

# Download all OTU tables: test
head -n2 allRuns_180416.tab > testRuns.tab
tail -n 3000 allRuns_180416.tab | head -n 200  >> testRuns.tab
python abundanceSurvey.py testRuns.tab /nobackup/ebi/metagenomics/OTUtables
# Download all OTU tables: real run
python abundanceSurvey.py allRuns_180416.tab /nobackup/ebi/metagenomics/OTUtables > logs/allRuns_log.180810 2> logs/allRuns_errorLog.180810

# Temp: rerun
grep "  Finished for" allRuns_log.180730 | tail -n10
head -n2 allRuns_180416.tab > allRunsRerun.tab
tail -n+201482 allRuns_180416.tab >> allRunsRerun.tab
python abundanceSurvey.py allRunsRerun.tab /nobackup/ebi/metagenomics/OTUtables > logs/allRunsRerun_log.180813 2> logs/allRuns_errorLog.180813 &

# Now fetch information by sample from both ebi and summarize run info.
python summarizeSample.py allSamples_180416.tab allLegioRuns_180416.tab /nobackup/ebi/metagenomics/OTUtables o__Legionellales summary.tab

# Rerunning with v3 OTU tables
python summarizeSample.py summary_cleaned.csv allLegioRuns_180416.tab /nobackup/ebi/metagenomics/OTUtables o__Legionellales summary_v3.tab

# Rerunning with v3 OTU tables
python summarizeSample.py summary_cleaned.csv allLegioRuns_180416.tab /nobackup/ebi/metagenomics/OTUtables o__Legionellales summary_v3.tab

# testing
head -n1 summary_cleaned.csv > testSample.tab
tail -n 1000 summary_cleaned.csv >> testSample.tab
head -n2 allLegioRuns_180416.tab > testRuns.tab
grep -f <(cut -f 1 testSample.tab | grep RS | sed -e 's/"//g') allLegioRuns_180416.tab  >> testRuns.tab
python summarizeSample.py testSample.tab testRuns.tab /nobackup/ebi/metagenomics/OTUtables o__Legionellales testSummary.tab

# Sorting biomes
cut -f7 summary_cleaned.csv | sort | uniq -c | sort -rn > biomes
cut -f7 summary_cleaned.csv | sort | uniq -c | sed -e "s/ root/\troot/" > biomes

# Extract Legionellales from greengenes
grep "o__Legionellales;" /nobackup/16Sdb/gg/gg_13_8_otus/taxonomy/99_otu_taxonomy.txt > gg_13_8_taxonomy_legio.txt

## Numbers
# all samples: 90861
# all legio runs: 50505
# samples for which we have info: 87955 87940
# samples for which we have Legion run: 20965

# Getting temperatures
python add_temperatures.py summary_v3.tab summary_v4.tab
# test:
head -n100 summary_v3.tab > testSamples.tab
python add_temperatures.py testSamples.tab summary_v4.tab

# Gammaprot orders
GG=/nobackup/16Sdb/gg/gg_13_8_otus/taxonomy
grep c__Gammaproteobacteria $GG/99_otu_taxonomy.txt | cut -f2 | cut -f4 -d";" | sed -e 's/ //g' | sort | uniq > gammaprot_orders


# Getting species and genera counts in Gammas
echo "Order\tCutoff 94%\tCutoff 97%\tCutoff 99%" > count_ge_sp.tab
while read ORDER; do COUNT94=$(grep $ORDER $GG/94_otu_taxonomy.txt | wc -l); COUNT97=$(grep $ORDER $GG/97_otu_taxonomy.txt | wc -l); COUNT99=$(grep $ORDER $GG/99_otu_taxonomy.txt | wc -l); echo -e "$ORDER\t$COUNT94\t$COUNT97\t$COUNT99" >> count_ge_sp.tab; done < gammaprot_orders 

# Summarizing the OTU tables for the following orders:
mkdir summary_per_order
sed -e 's/o__//' gammaprot_orders > gammaprot_orders_selected.bak
cat gammaprot_orders_selected | while read ORDER; do
    python summarizeSample.py summary_cleaned.csv allRuns_180416.tab /nobackup/ebi/metagenomics/OTUtables o__$ORDER summary_per_order/summary_$ORDER.tab > logs/summary_$ORDER.log 2> logs/summary_$ORDER.error.log
done


## Figures:
grep -v "#" figureTables_renames.tab | while read REF NEW WAS; do
    REFFULL=`ls figures/${REF}_*`
    NEWFULL=${REFFULL/$REF/$NEW}
    NEWFULL=${NEWFULL/figures/figuresRevision1}
    echo -e "Coyping $REFFULL to $NEWFULL; $WAS"
    cp $REFFULL $NEWFULL
done
