#!/usr/bin/env python
"""Summarizes data per sample

For each sample, get from EBI:
* Project/study ID
* Biome, lineage, feature, material, lat, long
* Name and abstract

Also, find all runs associated to the sample. Choose the most representative 
run by selecting the one that has the most OTUs (reads?)
For the selected run, summarize:
* Total reads
* Total OTUs
* Legio OTUs
* Legio reads
* For the 3 most abundant OTUs
  * ID
  * Reads belonging to that OTU
"""

## Libraries 
import sys
import os.path
import re
import pandas
import numpy as np
from jsonapi_client import Session, Filter

## Debug
import pprint

## Constants
API_BASE = 'https://www.ebi.ac.uk/metagenomics/api/latest/'

sample_columns = ['sample', 'project', 'name', 'description', 'biome',
                  'lineage', 'feature', 'material', 'latitude', 'longitude',
                  'temperature', 'temperature_unit']
run_columns = ['run', 'type', 'pipeline_version', 'total_reads', 'total_otus',
               'target_reads', 'target_otus']
debug = True
read_metadata_from_file = True

## General functions
def clean_string(string):
  """Replace non-alphanum characters by underscores"""
  if isinstance(string, str):
    return re.sub(r'([^\s\w]|_)+', ' ', string)
  else:
    return 'NA'

def valid_sample_name(string):
  """Checks that the sample name is S|E|D;RS;8-12 chars"""
  if re.match(r'[SED]RS\d{6,10}', string):
    return True
  else:
    return False
      
def create_result_df(n_most_abundant = 5):
  most_abundant_cols = []
  for i in range(n_most_abundant):
    most_abundant_cols.append('most_abundant_otu_' + str(i+1))
    most_abundant_cols.append('most_abundant_reads_' + str(i+1))
    
  df = pandas.DataFrame(columns = sample_columns + run_columns +
                        most_abundant_cols)
  return df
  
def prepare_row(sample, run, n_most_abundant = 5):
  # build a dict:
  row = {}
  for sc in sample_columns:
    row[sc] = getattr(sample, sc)
  for rc in run_columns:
    row[rc] = getattr(run, rc)
  for i in range(len(run.most_abundant_otus)):
    row['most_abundant_otu_' + str(i+1)] = run.most_abundant_otus[i]
    row['most_abundant_reads_' + str(i+1)] = run.most_abundant_reads[i]
  return row
      
def format_unit(unit):
    import html
    return html.unescape(unit) if unit else ""

## Classes
class Sample(object):
  """A class to hold samples
  
  Initiate with id, name, project, and description.
  Methods to get biome, lineage, feature, material, latitude, longitutde, 
  name, abstract, run
  """
  def __init__(self, id, project, name, description, biome = "", lineage = "",
               feature = "", material = "", latitude ="", longitude = "",
               temperature = "", temperature_unit = ""):
    self.sample = id
    self.name = clean_string(name)
    self.project = project
    self.description = clean_string(description)
    self.biome = biome
    self.lineage = lineage
    self.feature = feature
    self.material = material
    self.latitude = latitude    
    self.longitude = longitude
    self.temperature = temperature
    self.temperature_unit = temperature_unit

  def get_metadata(self, session):
    # params = {
    #   'study_accession': 'ERP001736',
    #   'include': 'biome',
    #   'page_size': 100,
    # }
    try: 
      sample = session.get('samples', self.sample).resource
      self.biome = sample.biome.biome_name
      self.lineage = sample.biome.lineage
      self.feature = sample.environment_feature
      self.material = sample.environment_material
      self.latitude = sample.latitude
      self.longitude = sample.longitude
    except:
      print("Error in sample: " + self.sample)
    
  def get_temperature(self, session):
    temperature = 'NA'
    unit = 'NA'
    try:
      sample = session.get('samples', self.sample).resource
      for m in sample.sample_metadata:
        if m['key'] == "temperature":
          temperature = m['value']
          unit = format_unit(m['unit'] or 'NA')
          break
    except:
      print("Something wrong with sample: " + self.sample)
          
    self.temperature = temperature
    self.temperature_unit = unit
        
      
class Run(object):
  """A class to hold runs
  
  Initiates with id, sample, project, type, pipeline_version
  Methods to get total reads/OTUs, Legio reads/OTUs, and for the five most
  abundant legio OTUs, the id and n reads
  """
  def __init__(self, id, sample = 'NA', project = 'NA', type = 'NA',
               pipeline_version = 'NA',
               total_reads = 'NA', total_otus = 'NA',
               target_reads = 'NA', target_otus = 'NA',
               most_abundant_reads = 'NA', most_abundant_otus = 'NA',
               n_most_abundant = 5):
    self.run = id
    self.sample = sample
    self.project = project
    self.type = type
    self.pipeline_version = pipeline_version
    self.total_reads = total_reads
    self.total_otus = total_otus
    self.target_reads = target_reads
    self.target_otus = target_otus
    self.most_abundant_reads = most_abundant_reads
    self.most_abundant_otus = most_abundant_otus

    if self.most_abundant_otus == 'NA':
      self.most_abundant_otus = ['NA'] * n_most_abundant

    if self.most_abundant_reads == 'NA':
      self.most_abundant_reads = ['NA'] * n_most_abundant
    

  def get_otu_data(self, otu_table_folder, target):
    """Get OTU statstics, creating an OtuTable object and analyzing it"""
    otu_table = OtuTable(self.run, self.type, self.pipeline_version)
    otu_table.get_df(otu_table_folder)
    if not otu_table.df is None:
      self.analyze_otus(otu_table.df, target)

  def analyze_otus(self, df, target, n_most_abundant = 5):
    """Calculates the following stats:

    total reads/OTUs, 
    Legio reads/OTUs, 
    for the five most abundant legio OTUs, the id and n reads
    """
    
    # Total stats
    self.total_reads = df['n'].sum()
    self.total_otus = df.shape[0]

    # Obtain the subset of df
    subset = df[df.taxonomy.str.contains(target)]
    if subset.shape[0] > 0:
      subset = subset.sort_values('n', ascending = False)
      # Target stats
      self.target_reads = subset['n'].sum()
      self.target_otus = subset.shape[0]
      # n most abundant otus
      self.most_abundant_otus = subset.index[:n_most_abundant].tolist()
      self.most_abundant_reads = subset.iloc[:n_most_abundant, 0].tolist()
    
class OtuTable(object):
  """Hold results of parsing an OTU table"""

  def __init__(self, runid, type = 'amplicon', pipeline_version = 1.0):
    self.runid = runid
    self.type = type
    self.pipeline_version = pipeline_version

  # format 1 is without headers, the read number and name as first column
  def open_df_format1(self, file):
    otus = pandas.read_table(
      filepath_or_buffer = file,
      header = None,
      sep = "\t",
      names = ('n_and_otu', 'taxonomy', 'confidence'),
      dtype = {'n_and_otu': str, 'taxonomy': str,
               'confidence': np.float64 },
      quotechar = '"',
      index_col = None
    )
    otus[['n','otu']] = otus['n_and_otu'].str.split(' ', expand=True)
    otus['n'] = pandas.to_numeric(otus['n'], errors = 'coerce')
    otus = otus.drop(columns = ['n_and_otu', 'confidence'])
    otus = otus[['otu', 'n', 'taxonomy']]
    otus = otus.set_index('otu')
    return otus

  # format 2 is the normal one, with headers
  def open_df_format2(self, file):
    otus = pandas.read_table(
      filepath_or_buffer = file,
      header = 0,
      names = ('otu', 'n', 'taxonomy'),
      dtype = {'otu': np.int32, 'n': np.int32, 'taxonomy': str},
      quotechar = '"',
      skiprows = 1,
      index_col = 0
    )
    return otus

  def is_format2_file(self, file):
    with open(file) as f:
      first_line = f.readline()
    if re.match('^#', first_line):
      return True
    else:
      return False

  def get_df(self, otu_table_folder):
    """Check if the OTU table exists, returns panda object if so, else None"""
    self.folder = otu_table_folder
    self.file = os.path.join(self.folder, self.runid + ".otu.tab")
    if os.path.exists(self.file) and os.path.getsize(self.file) > 0:
      if debug:
        print(self.file)
      # So, in version 1, formats are not consistent, so test it
      if self.pipeline_version < 2:
          if self.is_format2_file(self.file):
            self.df = self.open_df_format2(self.file)
          else:
            self.df = self.open_df_format1(self.file)
      else:
        self.df = self.open_df_format2(self.file)
    else:
      print("No otu table for", self.runid)
      self.df = None


## Main
def main():
  """Main function: opens files, go through samples"""
  # Parse options
  script_name = sys.argv.pop(0)
  sample_file = sys.argv.pop(0)
  run_file = sys.argv.pop(0)
  otu_table_folder = sys.argv.pop(0)
  target = sys.argv.pop(0)
  output_file = sys.argv.pop(0)
  print("Running: %s\n  read runs %s\n  read samples %s\n  write to %s"
        % (script_name, run_file, sample_file, output_file))
  print("  analyzing OTU tables in %s\n  targeting: %s"
        % (otu_table_folder, target))

  
  # Read runs and samples
  runs = pandas.read_table(filepath_or_buffer = run_file, header = 0,
                           quotechar = '"', skiprows = 1, index_col = 0)
  # If I read my samples from an already written file with metadata the 
  # formatting changes a bit

  if read_metadata_from_file:
    quoting = 0
    skiprows = 0
  else:
    quoting = 1
    skiprows = 1
    
  samples = pandas.read_table(filepath_or_buffer = sample_file, header = 0,
                              quotechar = '"', quoting = quoting,
                              doublequote = False,
                              skiprows = skiprows, index_col = 0)

  # Create an empty dict to hold representative runs
  rep_runs = {}

  # Group runs by sample
  grouped_runs = runs.groupby('Sample')
  # Process all runs per group, identify the most abundant
  for sample_id, group in grouped_runs:
    n_otus = []
    runs_by_sample = []
    print("Sample: %s" % sample_id)
    for r in group.itertuples():
      run = Run(r[0], r[1], r[2], r[3], r[4])
      if debug:
        print("  Run: %s\tSample: %s\tProject: %s\tType: %s\tPipeline: %s" 
              % (run.run, run.sample, run.project, run.type,
                 run.pipeline_version))
      run.get_otu_data(otu_table_folder, target)
      if debug:
        print("  Total reads: %s\tTarget reads: %s\tMost ab target reads: %s" 
              % (run.total_reads, run.target_reads,
                 run.most_abundant_otus))
      runs_by_sample.append(run)
      if run.total_otus == 'NA':
        n_otus.append(0)
      else:
        n_otus.append(run.total_otus)
    max_idx = n_otus.index(max(n_otus))
    rep_runs[sample_id] = runs_by_sample[max_idx]

  # Initiate result df panda object
  df = create_result_df()

  # Open session
  with Session(API_BASE) as session:

    # Process all samples
    for s in samples.itertuples():
      if not valid_sample_name(s[0]):
        print("Sample id not valid: " + s[0])
        next
      else:
        if read_metadata_from_file:
          sample = Sample(s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8],
                          s[9])
        else:
          sample = Sample(s[0], s[1], s[2], s[3])
          # print("Sample: %s\tProject: %s\tName: %s"
          #       % (sample.sample, sample.project, sample.name))
          sample.get_metadata(session)
          sample.get_temperature(session)
        # Create a fake run if not one present
        if not sample.sample in rep_runs:
          rep_runs[sample.sample] = Run('NA', sample.sample)
        if debug: 
          print("Sample: %s\tProject: %s\tName: %s\tBiome: %s\tMaterial: %s\tRepr run: %s"
                % (sample.sample, sample.project, sample.name, sample.biome,
                   sample.material, rep_runs[sample.sample].run))
        row = prepare_row(sample, rep_runs[sample.sample])
        df = df.append(row, ignore_index = True)
        
  # Replace 'NA' with nan.np
  df = df.replace(r'^NA$', np.nan, regex=True)
  # Create a new column to hold the fraction of target reads
  df['target_fraction'] = df['target_reads'].astype(np.float64)/ df['total_reads']
  # Sort by target_reads and write result
  df = df.sort_values(by='target_fraction', ascending = False)
  df.to_csv(output_file, sep="\t")
  
if __name__ == "__main__":
  main()
